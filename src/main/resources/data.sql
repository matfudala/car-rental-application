insert into users (username, password, enabled, email)
values ('admin', '$2a$10$hUhRqwjpeL8VULyu3vUuuui5.GTTmvur7Yx53MaHzmxEoqvZ298Ua', true, 'admin@mail.com'),
       ('user', '$2a$10$FIYwIkSDIDfupIAfgHsDu.cwdIB5CSeoBTrvgR2N7JDUGJ3E4EF6y', true, 'user@mail.com');

insert into role (name)
values ('ROLE_ADMIN'),
       ('ROLE_USER');

insert into users_roles (user_id, role_id)
values (1, 1),
       (1, 2),
       (2, 2);

insert into address (city, street, house_number, postal_code)
values ('Katowice', 'ul. Bukowa', '1A', '40-108'),
       ('Warszawa', 'ul. Łazienkowska', '3', '00-449'),
       ('Kraków', 'ul. Reymonta', '22', '31-409'),
       ('Poznań', 'ul. Bułgarska', '17/3', '60-320'),
       ('Gdańsk', 'ul. Pokoleń Lechii Gdańsk', '1', '80-560');


insert into customer (birth_date, email, first_name, last_name, address_id)
values ('1978-04-14', 'teddy.witting@mail.com', 'Teddy', 'Witting', 1),
       ('1976-01-16', 'bethel.hirthe@mail.com', 'Bethel', 'Hirthe', 2),
       ('1986-04-29', 'dinah.kirlin@mail.com', 'Dinah', 'Kirlin', 3),
       ('1964-09-10', 'leeann.haag@mail.com', 'Leeann', 'Haag', 4);


insert into department (department_Name, address_id)
values ('Katowice', 1),
       ('Warszawa', 2),
       ('Kraków', 3);

insert into car (make, model, car_type, production_year, car_color, mileage, rental_status, price_per_day,
                 department_id)
values ('Audi', 'A4', 'SEDAN', 2015, 'WHITE', 2000, 'AVAILABLE', 199.0, 1),
       ('VW', 'Passat', 'ESTATE', 2016, 'BLUE', 5000, 'AVAILABLE', 159.9, 1),
       ('Opel', 'Vectra', 'SEDAN', 2018, 'WHITE', 15000, 'AVAILABLE', 119.9, 2),
       ('Audi', 'A6', 'ESTATE', 2019, 'BLACK', 5000, 'AVAILABLE', 209.9, 3),
       ('VW', 'Golf', 'HATCHBACK', 2018, 'RED', 5000, 'AVAILABLE', 149.9, 1);

insert into department_cars(cars_id, department_id)
values (1, 1),
       (2, 1),
       (3, 2),
       (4, 3),
       (5, 1);

INSERT INTO reservation
(price, rental_end_date, rental_start_date, reservation_date, department_arrival_id, car_id, customer_id,
 department_departure_id)
VALUES (3582.00, '2020-01-19 12:00', '2020-01-01 12:00', '2020-01-13 07:45:16', 1, 1, 1, 1),
       (2878.20, '2020-01-19 12:00', '2020-01-01 12:00', '2020-01-13 07:45', 1, 2, 1, 1);

INSERT INTO rental(rental_date, returned, reservation_id)
VALUES ('2020-01-01 12:00:00', false, 2);

INSERT INTO rental_revenue(rental_value)
VALUES (2878.20);

INSERT INTO reservation_revenue(cancelled, realised, reservation_id, reservation_value)
VALUES (false, false, 1, 3582.00),
       (false, true, 2, 2878.20);

INSERT INTO unavailable (end_date, start_date, car_id) VALUES
('2020-01-10 12:00', '2020-01-01 11:00',5);

-- insert into rental_revenue(id, rental_value) values
-- (1,100),
-- (2,200),
-- (3,200),
-- (4,200),
-- (5,200);
--
-- insert into rental(id, additional_info, returned, rental_date, employee_id, reservation_id) values
-- (1,'DUPA','FALSE', '20200108 10:34:09 AM',null,NULL),
-- (2,'DUPA','FALSE', '20200108 10:34:09 AM',NULL,NULL),
-- (3,'DUPA','FALSE', '20200108 10:34:09 AM',NULL,NULL),
-- (4,'DUPA','FALSE', '20200108 10:34:09 AM',NULL,NULL),
-- (5,'DUPA','FALSE', '20200108 10:34:09 AM',NULL,NULL);
