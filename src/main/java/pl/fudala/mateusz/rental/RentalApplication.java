package pl.fudala.mateusz.rental;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.fudala.mateusz.rental.repository.CarRepository;

@SpringBootApplication
public class RentalApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(RentalApplication.class, args);
    }

    @Autowired
    CarRepository carRepository;

    @Override
    public void run(String... args){
    }

}
