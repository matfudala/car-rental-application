package pl.fudala.mateusz.rental.dto.revenues;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class RentalRevenueDto {
    private Long id;
    private BigDecimal rentalValue;
}
