package pl.fudala.mateusz.rental.dto.company;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.fudala.mateusz.rental.model.car.CarEntity;
import pl.fudala.mateusz.rental.model.company.AddressEntity;
import pl.fudala.mateusz.rental.model.person.EmployeeEntity;

import java.util.List;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class DepartmentDto {

    private Long id;
    private String departmentName;
    private AddressEntity address;
    private List<EmployeeEntity> employees;
    private List<CarEntity> cars;

    @Override
    public String toString() {
        return departmentName;
    }
}
