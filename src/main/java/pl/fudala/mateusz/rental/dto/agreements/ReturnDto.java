package pl.fudala.mateusz.rental.dto.agreements;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.fudala.mateusz.rental.model.agreements.RentalEntity;
import pl.fudala.mateusz.rental.model.person.EmployeeEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class ReturnDto {

    private Long id;
    private EmployeeEntity employee;
    private LocalDateTime returnDate;
    private RentalEntity rental;
    private BigDecimal surcharge;
    private String additionalInfo;

}
