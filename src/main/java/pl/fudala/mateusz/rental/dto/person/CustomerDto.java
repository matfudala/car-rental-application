package pl.fudala.mateusz.rental.dto.person;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import pl.fudala.mateusz.rental.model.agreements.ReservationEntity;
import pl.fudala.mateusz.rental.model.company.AddressEntity;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@Builder
public class CustomerDto {

    private Long id;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private String email;
    private AddressEntity address;
    private List<ReservationEntity> reservations;
}
