package pl.fudala.mateusz.rental.dto.car;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.fudala.mateusz.rental.model.agreements.ReservationEntity;
import pl.fudala.mateusz.rental.model.company.DepartmentEntity;
import pl.fudala.mateusz.rental.model.enums.CarColor;
import pl.fudala.mateusz.rental.model.enums.CarRentalStatus;
import pl.fudala.mateusz.rental.model.enums.CarType;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class CarDto {

    private Long id;
    private String model;
    private String make;
    private CarType carType;
    private Long productionYear;
    private CarColor carColor;
    private Long mileage;
    private CarRentalStatus rentalStatus;
    private BigDecimal pricePerDay;
    private DepartmentEntity department;
    private ReservationEntity reservation;

    public String getDetails() {
        return make + " " + model + " - " + pricePerDay + " per day";
    }

}
