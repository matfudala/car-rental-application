package pl.fudala.mateusz.rental.dto.agreements;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import pl.fudala.mateusz.rental.model.car.CarEntity;
import pl.fudala.mateusz.rental.model.company.DepartmentEntity;
import pl.fudala.mateusz.rental.model.person.CustomerEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class ReservationDto {

    private Long id;
    private LocalDateTime reservationDate;
    private CustomerEntity customer;
    private CarEntity car;
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime rentalEndDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime rentalStartDate;
    private DepartmentEntity departureDepartment;
    private DepartmentEntity arrivalDepartment;
    private BigDecimal price;

}
