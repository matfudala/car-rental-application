package pl.fudala.mateusz.rental.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.fudala.mateusz.rental.dto.agreements.ReservationDto;
import pl.fudala.mateusz.rental.dto.car.CarDto;
import pl.fudala.mateusz.rental.model.car.CarEntity;
import pl.fudala.mateusz.rental.model.company.DepartmentEntity;
import pl.fudala.mateusz.rental.model.enums.CarRentalStatus;
import pl.fudala.mateusz.rental.model.revenues.ReservationRevenueEntity;
import pl.fudala.mateusz.rental.repository.CarRepository;
import pl.fudala.mateusz.rental.repository.ReservationRevenueRepository;
import pl.fudala.mateusz.rental.service.*;
import pl.fudala.mateusz.rental.utils.mapper.Mapper;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping(ReservationController.BASE_URL)
public class ReservationController {

    public static final String BASE_URL = "/api/reservations";

    private final ReservationService reservationService;
    private final DepartmentService departmentService;
    private final CarService carService;
    private final CustomerService customerService;
    private final CarRepository carRepository;
    private final RentalService rentalService;
    private final ReservationRevenueService reservationRevenueService;
    private final ReservationRevenueRepository reservationRevenueRepository;

    @Autowired
    public ReservationController(ReservationService reservationService, DepartmentService departmentService,
                                 CarService carService, CustomerService customerService, CarRepository carRepository, RentalService rentalService, ReservationRevenueService reservationRevenueService, ReservationRevenueRepository reservationRevenueRepository) {
        this.reservationService = reservationService;
        this.departmentService = departmentService;
        this.carService = carService;
        this.customerService = customerService;
        this.carRepository = carRepository;
        this.rentalService = rentalService;
        this.reservationRevenueService = reservationRevenueService;
        this.reservationRevenueRepository = reservationRevenueRepository;
    }

    @GetMapping
    ModelAndView allReservations() {
        ModelAndView mav = new ModelAndView("rentals/reservations");
        mav.addObject("reservations", reservationService.findAllNotRealised());
        return mav;
    }

    @GetMapping("/create")
    ModelAndView getAvailableCarsByDepartmentId(@RequestParam(value = "dept", required = false)
                                                        Long departureDepartmentId) {
        ModelAndView mav = new ModelAndView("rentals/create-reservation");
        if (departureDepartmentId != null) {
            mav.addObject("reservation", new ReservationDto());
            mav.addObject("cars",
                    carService.findAvailableCarsByDepartment(departureDepartmentId));
            mav.addObject("arrivalDepartments", departmentService.findAll());
            mav.addObject("departureDepartments", departmentService.findDepartmentsWithAvailableCars());
            mav.addObject("deptName", departmentService
                    .findById(departureDepartmentId).getDepartmentName());
        } else {
            mav.addObject("reservation", new ReservationDto());
            mav.addObject("departureDepartments", departmentService.findDepartmentsWithAvailableCars());
        }
        return mav;
    }

    @PostMapping("/create")
    ModelAndView createReservation(@ModelAttribute("reservation") ReservationDto reservationDto) {

        reservationDto.setDepartureDepartment(reservationDto.getCar().getDepartment());
        reservationDto.setReservationDate(LocalDateTime.now());
        reservationDto.setPrice(reservationService.calculatePrice(reservationDto));
        reservationDto.getCar().setRentalStatus(CarRentalStatus.RESERVED);
        reservationDto.setCustomer(Mapper.map(customerService.findById(1L)));
        reservationService.addReservation(reservationDto);

        return new ModelAndView("redirect:" + BASE_URL);
    }

    @GetMapping("/delete/{id}")
    ModelAndView deleteReservation(@PathVariable Long id) {

        ReservationDto reservationDto = reservationService.findById(id);
        CarEntity car = reservationDto.getCar();
        car.setRentalStatus(CarRentalStatus.AVAILABLE);
        carRepository.save(car);
        ReservationRevenueEntity resRev = reservationRevenueRepository.getReservationRevenueByReservationId(id);
        resRev.setCancelled(true);
        reservationService.deleteReservationById(id);

        return new ModelAndView("redirect:" + BASE_URL);
    }

    @GetMapping("/convert/{id}")
    ModelAndView getRentals(@PathVariable Long id) {
        ModelAndView mav = new ModelAndView("rentals/reservations");
        rentalService.convertReservationToRental(id, "");
        mav.addObject("rentals", rentalService.getAllRentals());
        return mav;
    }

    @GetMapping("/edit/{id}")
    ModelAndView getEditPage(@PathVariable Long id, @RequestParam(value = "dept") Optional<Long> departureDepartmentId) {
        ModelAndView mav = new ModelAndView("rentals/edit-reservation");
        if (departureDepartmentId.isPresent()) {
            ReservationDto reservation = reservationService.findById(id);
            reservationService.findById(id).getCar().setRentalStatus(CarRentalStatus.AVAILABLE);
            mav.addObject("reservation", reservation);
            List<CarDto> availableCarsByDepartment = carService.findAvailableCarsByDepartment(departureDepartmentId
                    .orElseThrow(IllegalArgumentException::new));

            mav.addObject("cars", availableCarsByDepartment);
            mav.addObject("arrivalDepartments", departmentService.findAll());
            mav.addObject("departureDepartments", departmentService.findAll());
            mav.addObject("deptName", departmentService
                    .findById(departureDepartmentId.orElseThrow(IllegalArgumentException::new)));

        } else {

            ReservationDto reservation = reservationService.findById(id);
            carService.findById(reservation.getCar().getId()).setRentalStatus(CarRentalStatus.AVAILABLE);
            mav.addObject("deptName", reservation.getDepartureDepartment().getDepartmentName());
            mav.addObject("departureDepartment", reservation.getDepartureDepartment());
            mav.addObject("cars", reservation.getDepartureDepartment()
                    .getCars()
                    .stream()
                    .map(Mapper::map)
                    .collect(Collectors.toList()));
            mav.addObject("reservation", reservation);
            mav.addObject("departureDepartments", departmentService.findAll());
            mav.addObject("arrivalDepartment", reservation.getArrivalDepartment());
            mav.addObject("arrivalDepartments", departmentService.findAll());

        }


        return mav;
    }

    @PostMapping("/edit/{id}")
    ModelAndView editReservation(@ModelAttribute("reservation") ReservationDto reservationDto) {
        ModelAndView mav = new ModelAndView("rentals/reservations");

        reservationService.editReservation(reservationDto);

        mav.addObject("reservations", reservationService.findAllNotRealised());
        return mav;
    }


}
