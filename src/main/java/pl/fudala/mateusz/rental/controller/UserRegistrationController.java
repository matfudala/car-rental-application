package pl.fudala.mateusz.rental.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import pl.fudala.mateusz.rental.dto.users.UserRegistrationDto;
import pl.fudala.mateusz.rental.model.users.User;
import pl.fudala.mateusz.rental.service.UserService;

import javax.validation.Valid;

@RestController
@RequestMapping("/registration")
public class UserRegistrationController {
    @Autowired
    private UserService userService;

    @PostMapping
    public ModelAndView registerUserAccount(@ModelAttribute("user") @Valid UserRegistrationDto userRegistrationDto,
                                            BindingResult result) {
        User existing = userService.findByUsername(userRegistrationDto.getUsername());
        if (existing != null) {
            return new ModelAndView("redirect:login?exists");
        }
        if (result.hasErrors()) {
            return new ModelAndView("redirect:login?incorrect");
        }

        userService.save(userRegistrationDto);
        return new ModelAndView("redirect:login?success");

    }

}
