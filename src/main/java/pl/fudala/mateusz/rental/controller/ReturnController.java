package pl.fudala.mateusz.rental.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.fudala.mateusz.rental.dto.agreements.RentalDto;
import pl.fudala.mateusz.rental.service.RentalService;
import pl.fudala.mateusz.rental.service.ReturnService;

import java.util.Optional;

import static pl.fudala.mateusz.rental.controller.ReturnController.BASE_URL;


@Controller
@RequestMapping(BASE_URL)
public class ReturnController {

    public static final String BASE_URL = "api/returns";

    private final ReturnService returnService;
    private final RentalService rentalService;

    @Autowired
    public ReturnController(ReturnService returnService, RentalService rentalService) {
        this.returnService = returnService;
        this.rentalService = rentalService;
    }

    @GetMapping("/{id}")
    ModelAndView rentalPage(@PathVariable Optional<Long> id) {
        ModelAndView mav = new ModelAndView("rentals/rentals");
        RentalDto rentalDto = rentalService.getRentalById(id.orElseThrow(IllegalArgumentException::new));
        returnService.returnCar(rentalDto);
        mav.addObject("rentals", rentalService.getAllRentals());
        return mav;
    }

    @GetMapping
    ModelAndView returnsPage() {
        ModelAndView mav = new ModelAndView("rentals/returns");
        mav.addObject("returns", returnService.allReturns());
        return mav;
    }
}
