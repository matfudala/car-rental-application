package pl.fudala.mateusz.rental.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import pl.fudala.mateusz.rental.service.RentalService;

@Controller
@RequestMapping(RentalController.BASE_URL)
public class RentalController {

    public static final String BASE_URL = "/api/rentals";

    private final RentalService rentalService;

    @Autowired
    public RentalController(RentalService rentalService) {
        this.rentalService = rentalService;
    }

    @GetMapping()
    public ModelAndView getRentals() {
        ModelAndView mav = new ModelAndView("rentals/rentals");
        mav.addObject("rentals", rentalService.getAllRentals());
        return mav;
    }
}
