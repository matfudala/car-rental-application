package pl.fudala.mateusz.rental.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import pl.fudala.mateusz.rental.dto.revenues.RentalRevenueDto;
import pl.fudala.mateusz.rental.dto.revenues.ReservationRevenueDto;
import pl.fudala.mateusz.rental.dto.revenues.ReturnRevenueDto;
import pl.fudala.mateusz.rental.service.RentalRevenueService;
import pl.fudala.mateusz.rental.service.ReservationRevenueService;
import pl.fudala.mateusz.rental.service.ReturnRevenueService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping(RevenuesController.BASE_URL)
public class RevenuesController {

    public static final String BASE_URL = "api/revenues";

    private final ReservationRevenueService reservationRevenueService;
    private final RentalRevenueService rentalRevenueService;
    private final ReturnRevenueService returnRevenueService;

    @Autowired
    public RevenuesController(ReservationRevenueService reservationRevenueService, RentalRevenueService rentalRevenueService, ReturnRevenueService returnRevenueService) {
        this.reservationRevenueService = reservationRevenueService;
        this.rentalRevenueService = rentalRevenueService;
        this.returnRevenueService = returnRevenueService;
    }

    @GetMapping
    public ModelAndView allRevenues() {
        return new ModelAndView("statistics/revenues");
    }

    @GetMapping("/rental")
    ModelAndView rentalRevenues(@RequestParam Optional<Integer> page, @RequestParam Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(2);

        Page<RentalRevenueDto> revenues = rentalRevenueService
                .findPaginated(PageRequest.of(currentPage - 1, pageSize));
        ModelAndView mav = new ModelAndView("statistics/rental-revenues");
        checkTotalPages(revenues, mav);

        mav.addObject("revenues", revenues);
        mav.addObject("rentalSum", rentalRevenueService.getSumOfRentalRevenues());
        mav.addObject("sum", rentalRevenueService.getSumOfAllRevenues());
        return mav;
    }

    @GetMapping("/reservation")
    ModelAndView reservationRevenues(@RequestParam Optional<Integer> page, @RequestParam Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(2);

        Page<ReservationRevenueDto> revenues = reservationRevenueService
                .findPaginated(PageRequest.of(currentPage - 1, pageSize));
        ModelAndView mav = new ModelAndView("statistics/reservation-revenues");
        checkTotalPages(revenues, mav);

        mav.addObject("revenues", revenues);
        mav.addObject("reservationSumActive", reservationRevenueService.getSumOfActiveReservationRevenues());
        mav.addObject("reservationSumCancelled", reservationRevenueService.getSumOfCancelledReservationRevenues());
        mav.addObject("reservationSum", reservationRevenueService.getSumOfReservationRevenues());
        mav.addObject("sum", rentalRevenueService.getSumOfAllRevenues());
        return mav;
    }


    @GetMapping("/return")
    ModelAndView returnRevenues(@RequestParam Optional<Integer> page) {
        int pageNumber = page.orElse(1);
        int pageSize = 2;

        Page<ReturnRevenueDto> revenues = returnRevenueService
                .findAll(PageRequest.of(pageNumber - 1, pageSize, Sort.by("id")));
        ModelAndView mav = new ModelAndView("statistics/return-revenues");
        checkTotalPages(revenues, mav);
        mav.addObject("revenues", revenues);
        mav.addObject("returnsSum", returnRevenueService.sumOfSurcharges());
        mav.addObject("sum", rentalRevenueService.getSumOfAllRevenues());
        return mav;
    }


    private void checkTotalPages(Page<?> revenues, ModelAndView mav) {
        int totalPages = revenues.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            mav.addObject("pageNumbers", pageNumbers);
        }
    }


}
