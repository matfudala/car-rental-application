package pl.fudala.mateusz.rental.utils.mapper;


import pl.fudala.mateusz.rental.dto.agreements.RentalDto;
import pl.fudala.mateusz.rental.dto.agreements.ReservationDto;
import pl.fudala.mateusz.rental.dto.agreements.ReturnDto;
import pl.fudala.mateusz.rental.dto.agreements.UnavailableDto;
import pl.fudala.mateusz.rental.dto.car.CarDto;
import pl.fudala.mateusz.rental.dto.company.DepartmentDto;
import pl.fudala.mateusz.rental.dto.company.HeadquarterDto;
import pl.fudala.mateusz.rental.dto.person.AddressDto;
import pl.fudala.mateusz.rental.dto.person.CustomerDto;
import pl.fudala.mateusz.rental.dto.person.EmployeeDto;
import pl.fudala.mateusz.rental.dto.revenues.RentalRevenueDto;
import pl.fudala.mateusz.rental.dto.revenues.ReservationRevenueDto;
import pl.fudala.mateusz.rental.dto.revenues.ReturnRevenueDto;
import pl.fudala.mateusz.rental.model.agreements.RentalEntity;
import pl.fudala.mateusz.rental.model.agreements.ReservationEntity;
import pl.fudala.mateusz.rental.model.agreements.ReturnEntity;
import pl.fudala.mateusz.rental.model.agreements.UnavailableEntity;
import pl.fudala.mateusz.rental.model.car.CarEntity;
import pl.fudala.mateusz.rental.model.company.AddressEntity;
import pl.fudala.mateusz.rental.model.company.DepartmentEntity;
import pl.fudala.mateusz.rental.model.company.HeadquarterEntity;
import pl.fudala.mateusz.rental.model.person.CustomerEntity;
import pl.fudala.mateusz.rental.model.person.EmployeeEntity;
import pl.fudala.mateusz.rental.model.revenues.RentalRevenueEntity;
import pl.fudala.mateusz.rental.model.revenues.ReservationRevenueEntity;
import pl.fudala.mateusz.rental.model.revenues.ReturnRevenueEntity;

public class Mapper {

    private Mapper() {}

    public static RentalDto map(RentalEntity rentalEntity) {
        return RentalDto.builder().additionalInfo(rentalEntity.getAdditionalInfo())
                .employee(rentalEntity.getEmployee())
                .id(rentalEntity.getId())
                .reservation(rentalEntity.getReservation())
                .rentalDate(rentalEntity.getRentalDate())
                .returned(rentalEntity.isReturned())
                .build();
    }

    public static RentalEntity map(RentalDto rentalDto) {
        return RentalEntity.builder().additionalInfo(rentalDto.getAdditionalInfo())
                .employee(rentalDto.getEmployee())
                .id(rentalDto.getId())
                .reservation(rentalDto.getReservation())
                .rentalDate(rentalDto.getRentalDate())
                .returned(rentalDto.isReturned())
                .build();
    }

    public static ReservationDto map(ReservationEntity reservationEntity) {
        return ReservationDto.builder().arrivalDepartment(reservationEntity.getArrivalDepartment())
                .car(reservationEntity.getCar())
                .customer(reservationEntity.getCustomer())
                .departureDepartment(reservationEntity.getDepartureDepartment())
                .id(reservationEntity.getId())
                .price(reservationEntity.getPrice())
                .rentalEndDate(reservationEntity.getRentalEndDate())
                .rentalStartDate(reservationEntity.getRentalStartDate())
                .reservationDate(reservationEntity.getReservationDate())
                .build();
    }

    public static ReservationEntity map(ReservationDto reservationDto) {
        return ReservationEntity.builder().arrivalDepartment(reservationDto.getArrivalDepartment())
                .car(reservationDto.getCar())
                .customer(reservationDto.getCustomer())
                .departureDepartment(reservationDto.getDepartureDepartment())
                .id(reservationDto.getId())
                .price(reservationDto.getPrice())
                .rentalEndDate(reservationDto.getRentalEndDate())
                .rentalStartDate(reservationDto.getRentalStartDate())
                .reservationDate(reservationDto.getReservationDate())
                .build();
    }

    public static ReturnDto map(ReturnEntity returnEntity) {
        return ReturnDto.builder().additionalInfo(returnEntity.getAdditionalInfo())
                .employee(returnEntity.getEmployee())
                .rental(returnEntity.getRental())
                .surcharge(returnEntity.getSurcharge())
                .returnDate(returnEntity.getReturnDate())
                .id(returnEntity.getId())
                .build();
    }

    public static ReturnEntity map(ReturnDto returnDto) {
        return ReturnEntity.builder().additionalInfo(returnDto.getAdditionalInfo())
                .employee(returnDto.getEmployee())
                .rental(returnDto.getRental())
                .surcharge(returnDto.getSurcharge())
                .returnDate(returnDto.getReturnDate())
                .id(returnDto.getId())
                .build();
    }

    public static CarDto map(CarEntity carEntity) {
        return CarDto.builder().id(carEntity.getId())
                .reservation(carEntity.getReservation())
                .carColor(carEntity.getCarColor())
                .carType(carEntity.getCarType())
                .make(carEntity.getMake())
                .department(carEntity.getDepartment())
                .mileage(carEntity.getMileage())
                .pricePerDay(carEntity.getPricePerDay())
                .model(carEntity.getModel())
                .productionYear(carEntity.getProductionYear())
                .rentalStatus(carEntity.getRentalStatus())
                .build();
    }

    public static CarEntity map(CarDto carDto) {
        return CarEntity.builder().id(carDto.getId())
                .reservation(carDto.getReservation())
                .carColor(carDto.getCarColor())
                .carType(carDto.getCarType())
                .make(carDto.getMake())
                .department(carDto.getDepartment())
                .mileage(carDto.getMileage())
                .pricePerDay(carDto.getPricePerDay())
                .model(carDto.getModel())
                .productionYear(carDto.getProductionYear())
                .rentalStatus(carDto.getRentalStatus())
                .build();
    }

    public static DepartmentDto map(DepartmentEntity departmentEntity) {
        return DepartmentDto.builder().id(departmentEntity.getId())
                .address(departmentEntity.getAddress())
                .cars(departmentEntity.getCars())
                .departmentName(departmentEntity.getDepartmentName())
                .employees(departmentEntity.getEmployees())
                .build();
    }

    public static DepartmentEntity map(DepartmentDto departmentDto) {
        return DepartmentEntity.builder().id(departmentDto.getId())
                .address(departmentDto.getAddress())
                .cars(departmentDto.getCars())
                .departmentName(departmentDto.getDepartmentName())
                .employees(departmentDto.getEmployees())
                .build();
    }

    public static HeadquarterDto map(HeadquarterEntity headquarterEntity) {
        return HeadquarterDto.builder().name(headquarterEntity.getName())
                .address(headquarterEntity.getAddress())
                .companyLogo(headquarterEntity.getCompanyLogo())
                .departments(headquarterEntity.getDepartments())
                .website(headquarterEntity.getWebsite())
                .address(headquarterEntity.getAddress())
                .address(headquarterEntity.getAddress())
                .build();
    }

    public static HeadquarterEntity map(HeadquarterDto headquarterDto) {
        return HeadquarterEntity.builder().name(headquarterDto.getName())
                .address(headquarterDto.getAddress())
                .companyLogo(headquarterDto.getCompanyLogo())
                .departments(headquarterDto.getDepartments())
                .website(headquarterDto.getWebsite())
                .address(headquarterDto.getAddress())
                .address(headquarterDto.getAddress())
                .build();
    }

    public static AddressDto map(AddressEntity addressEntity) {
        return AddressDto.builder().id(addressEntity.getId())
                .city(addressEntity.getCity())
                .houseNumber(addressEntity.getHouseNumber())
                .postalCode(addressEntity.getPostalCode())
                .street(addressEntity.getStreet())
                .build();
    }

    public static AddressEntity map(AddressDto addressDto) {
        return AddressEntity.builder().id(addressDto.getId())
                .city(addressDto.getCity())
                .houseNumber(addressDto.getHouseNumber())
                .postalCode(addressDto.getPostalCode())
                .street(addressDto.getStreet())
                .build();
    }

    public static CustomerDto map(CustomerEntity customerEntity) {
        return CustomerDto.builder().id(customerEntity.getId())
                .address(customerEntity.getAddress())
                .birthDate(customerEntity.getBirthDate())
                .email(customerEntity.getEmail())
                .firstName(customerEntity.getFirstName())
                .lastName(customerEntity.getLastName())
                .reservations(customerEntity.getReservations())
                .build();
    }

    public static CustomerEntity map(CustomerDto customerDto) {
        return CustomerEntity.builder().id(customerDto.getId())
                .address(customerDto.getAddress())
                .birthDate(customerDto.getBirthDate())
                .email(customerDto.getEmail())
                .firstName(customerDto.getFirstName())
                .lastName(customerDto.getLastName())
                .reservations(customerDto.getReservations())
                .build();
    }

    public static EmployeeDto map(EmployeeEntity employeeEntity) {
        return EmployeeDto.builder().address(employeeEntity.getAddress())
                .id(employeeEntity.getId())
                .birthDate(employeeEntity.getBirthDate())
                .department(employeeEntity.getDepartment())
                .jobTitle(employeeEntity.getJobTitle())
                .firstName(employeeEntity.getFirstName())
                .lastName(employeeEntity.getLastName())
                .rentals(employeeEntity.getRentals())
                .build();
    }

    public static EmployeeEntity map(EmployeeDto employeeDto) {
        return EmployeeEntity.builder().address(employeeDto.getAddress())
                .id(employeeDto.getId())
                .birthDate(employeeDto.getBirthDate())
                .department(employeeDto.getDepartment())
                .jobTitle(employeeDto.getJobTitle())
                .firstName(employeeDto.getFirstName())
                .lastName(employeeDto.getLastName())
                .rentals(employeeDto.getRentals())
                .build();
    }


    public static RentalRevenueEntity map(RentalRevenueDto rentalRevenueDto) {
        return RentalRevenueEntity.builder().id(rentalRevenueDto.getId())
                .rentalValue(rentalRevenueDto.getRentalValue())
                .build();
    }

    public static RentalRevenueDto map(RentalRevenueEntity rentalRevenueEntity) {
        return RentalRevenueDto.builder().id(rentalRevenueEntity.getId())
                .rentalValue(rentalRevenueEntity.getRentalValue())
                .build();
    }

    public static ReservationRevenueEntity map(ReservationRevenueDto reservationRevenueDto) {
        return ReservationRevenueEntity.builder().cancelled(reservationRevenueDto.isCancelled())
                .id(reservationRevenueDto.getId())
                .realised(reservationRevenueDto.isRealised())
                .reservationValue(reservationRevenueDto.getReservationValue())
                .build();
    }

    public static ReservationRevenueDto map(ReservationRevenueEntity reservationRevenueEntity) {
        return ReservationRevenueDto.builder().cancelled(reservationRevenueEntity.isCancelled())
                .id(reservationRevenueEntity.getId())
                .realised(reservationRevenueEntity.isRealised())
                .reservationValue(reservationRevenueEntity.getReservationValue())
                .build();
    }

    public static ReturnRevenueEntity map(ReturnRevenueDto returnRevenueDto) {
        return ReturnRevenueEntity.builder().id(returnRevenueDto.getId())
                .returnSurcharge(returnRevenueDto.getReturnSurcharge())
                .build();
    }

    public static ReturnRevenueDto map(ReturnRevenueEntity returnRevenueEntity) {
        return ReturnRevenueDto.builder().id(returnRevenueEntity.getId())
                .returnSurcharge(returnRevenueEntity.getReturnSurcharge())
                .build();
    }

    public static UnavailableDto map(UnavailableEntity unavailableEntity){
        return UnavailableDto.builder()
                .car(unavailableEntity.getCar())
                .startDate(unavailableEntity.getStartDate())
                .endDate(unavailableEntity.getEndDate())
                .id(unavailableEntity.getId())
                .build();
    }

    public static UnavailableEntity map(UnavailableDto unavailableDto){
        return UnavailableEntity.builder()
                .car(unavailableDto.getCar())
                .startDate(unavailableDto.getStartDate())
                .endDate(unavailableDto.getEndDate())
                .id(unavailableDto.getId())
                .build();
    }
}
