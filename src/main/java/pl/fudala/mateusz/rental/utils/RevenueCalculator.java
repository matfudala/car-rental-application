package pl.fudala.mateusz.rental.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.fudala.mateusz.rental.model.agreements.ReturnEntity;
import pl.fudala.mateusz.rental.repository.RentalRepository;
import pl.fudala.mateusz.rental.repository.ReturnRepository;

import java.math.BigDecimal;

@Component
public class RevenueCalculator {

    @Autowired
    RentalRepository rentalRepository;

    @Autowired
    ReturnRepository returnRepository;

    public BigDecimal calculateRevenue() {
        BigDecimal reservationRevenue = rentalRepository.findAll().stream()
                .map(r -> r.getReservation().getPrice())
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        BigDecimal returnSurcharge = returnRepository.findAll().stream()
                .map(ReturnEntity::getSurcharge)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return reservationRevenue.add(returnSurcharge);
    }
}
