package pl.fudala.mateusz.rental.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.fudala.mateusz.rental.model.revenues.ReservationRevenueEntity;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Repository
public interface ReservationRevenueRepository extends JpaRepository<ReservationRevenueEntity, Long> {

    @Transactional
    @Query(value = "SELECT SUM(SUM) FROM " +
            "( " +
            "SELECT SUM(reservation_revenue.reservation_value) * 0.2 " +
            "as SUM FROM reservation_revenue WHERE cancelled = 'TRUE' " +
            "AND realised = 'FALSE' " +
            "UNION ALL " +
            "SELECT SUM(reservation_revenue.reservation_value) " +
            "as SUM FROM reservation_revenue WHERE cancelled = 'FALSE' " +
            "AND realised = 'FALSE' " +
            ") SUM ;", nativeQuery = true)
    Optional<BigDecimal> getSumOfReservationRevenues();

    @Query(value = "SELECT SUM(reservationValue) * 0.2 " +
            "as SUM FROM reservation_revenue rr WHERE rr.cancelled = 'TRUE' AND rr.realised = 'FALSE' ")
    Optional<BigDecimal> getSumOfCancelledRevenues();

    @Query(value = "SELECT SUM(reservationValue) " +
            "as SUM FROM reservation_revenue rr WHERE rr.cancelled = 'FALSE' AND rr.realised = 'FALSE' ")
    Optional<BigDecimal> getSumOfActiveReservationRevenues();


    @Query(value = "SELECT rr FROM reservation_revenue rr WHERE rr.cancelled = 'TRUE' AND rr.realised = 'FALSE' ")
    List<ReservationRevenueEntity> getCancelledReservationRevenues();

    @Query(value = "SELECT rr FROM reservation_revenue rr WHERE rr.cancelled = 'FALSE' AND rr.realised = 'FALSE' ")
    List<ReservationRevenueEntity> getActiveReservationRevenues();

    @Query(value = "SELECT rr FROM reservation_revenue rr WHERE reservationId = :id ")
    ReservationRevenueEntity getReservationRevenueByReservationId(@Param(value = "id") Long id);


}
