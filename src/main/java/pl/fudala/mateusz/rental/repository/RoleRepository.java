package pl.fudala.mateusz.rental.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.fudala.mateusz.rental.model.users.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByName(String name);
}
