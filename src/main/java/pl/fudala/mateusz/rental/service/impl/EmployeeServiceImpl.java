package pl.fudala.mateusz.rental.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import pl.fudala.mateusz.rental.dto.person.EmployeeDto;
import pl.fudala.mateusz.rental.repository.EmployeeRepository;
import pl.fudala.mateusz.rental.service.EmployeeService;
import pl.fudala.mateusz.rental.utils.mapper.Mapper;

public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public void add(EmployeeDto employeeDto) {
        employeeRepository.save(Mapper.map(employeeDto));
    }

    @Override
    public Page<EmployeeDto> findAll(Pageable pageable) {
        return employeeRepository.findAll(pageable).map(Mapper::map);
    }

    @Override
    public EmployeeDto findById(Long id) {
        return Mapper.map(employeeRepository.findById(id).orElseThrow(IllegalArgumentException::new));
    }

    @Override
    public void deleteById(Long id) {
        employeeRepository.deleteById(id);
    }
}
