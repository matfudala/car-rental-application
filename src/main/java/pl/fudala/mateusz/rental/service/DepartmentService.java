package pl.fudala.mateusz.rental.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import pl.fudala.mateusz.rental.dto.company.DepartmentDto;

import java.util.List;

public interface DepartmentService {
    List<DepartmentDto> findAll();
    Page<DepartmentDto> findAll(Pageable pageable);
    DepartmentDto findById(Long id);
    void addNewDepartment(DepartmentDto departmentDto);
    void editDepartment(DepartmentDto departmentDto, Long id);
    void deleteDepartmentById(Long id);
    List<DepartmentDto> findDepartmentsWithAvailableCars();
}
