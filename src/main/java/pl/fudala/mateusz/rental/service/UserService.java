package pl.fudala.mateusz.rental.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import pl.fudala.mateusz.rental.dto.users.UserRegistrationDto;
import pl.fudala.mateusz.rental.model.users.User;

public interface UserService extends UserDetailsService {
    User findByUsername(String username);
    User save(UserRegistrationDto registrationDto);
}
