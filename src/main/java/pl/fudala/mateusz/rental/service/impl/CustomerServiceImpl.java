package pl.fudala.mateusz.rental.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.fudala.mateusz.rental.dto.person.CustomerDto;
import pl.fudala.mateusz.rental.model.person.CustomerEntity;
import pl.fudala.mateusz.rental.repository.CustomerRepository;
import pl.fudala.mateusz.rental.service.CustomerService;
import pl.fudala.mateusz.rental.utils.mapper.Mapper;

@Service
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public CustomerDto findById(Long id) {
        CustomerEntity customerEntity = customerRepository.findById(id)
                .orElseThrow(IllegalArgumentException::new);
        return Mapper.map(customerEntity);
    }
}
