package pl.fudala.mateusz.rental.service;

import pl.fudala.mateusz.rental.dto.agreements.RentalDto;
import java.util.List;

public interface RentalService {

    void convertReservationToRental(Long id, String message);

    List<RentalDto> getAllRentals();

    RentalDto getRentalById(Long id);
}
