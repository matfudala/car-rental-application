package pl.fudala.mateusz.rental.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.fudala.mateusz.rental.dto.agreements.RentalDto;
import pl.fudala.mateusz.rental.dto.agreements.ReturnDto;
import pl.fudala.mateusz.rental.model.agreements.ReturnEntity;
import pl.fudala.mateusz.rental.model.enums.CarRentalStatus;
import pl.fudala.mateusz.rental.repository.RentalRepository;
import pl.fudala.mateusz.rental.repository.ReturnRepository;
import pl.fudala.mateusz.rental.service.ReturnService;
import pl.fudala.mateusz.rental.utils.mapper.Mapper;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReturnServiceImpl implements ReturnService {

    private final ReturnRepository returnRepository;
    private final RentalRepository rentalRepository;

    @Autowired
    public ReturnServiceImpl(ReturnRepository returnRepository, RentalRepository rentalRepository) {
        this.returnRepository = returnRepository;
        this.rentalRepository = rentalRepository;
    }

    @Override
    public void returnCar(RentalDto rentalDto) {
        ReturnEntity returnEntity = new ReturnEntity();
        returnEntity.setReturnDate(LocalDateTime.now());
        returnEntity.setRental(rentalRepository.findById(rentalDto.getId())
                .orElseThrow(IllegalArgumentException::new));
        rentalDto.getReservation().getCar().setRentalStatus(CarRentalStatus.AVAILABLE);
        returnEntity.setSurcharge(calculateSurcharge(returnEntity, rentalDto));
        rentalRepository.findById(rentalDto.getId())
                .orElseThrow(IllegalArgumentException::new).setReturned(true);
        returnRepository.save(returnEntity);
    }

    @Override
    public List<ReturnDto> allReturns() {
        return returnRepository.findAll().stream().map(Mapper::map).collect(Collectors.toList());
    }

    private BigDecimal calculateSurcharge(ReturnEntity returnEntity, RentalDto rentalDto) {
        LocalDateTime returnDate = returnEntity.getReturnDate();
        LocalDateTime rentalEndDate = rentalDto.getReservation().getRentalEndDate();
        long daysToSurcharge = Duration.between(rentalEndDate, returnDate).toDays();
        if (daysToSurcharge > 0) {
            return new BigDecimal(daysToSurcharge)
                    .multiply(rentalDto.getReservation().getCar().getPricePerDay());
        }
        return BigDecimal.ZERO;
    }
}
