package pl.fudala.mateusz.rental.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import pl.fudala.mateusz.rental.dto.revenues.RentalRevenueDto;
import pl.fudala.mateusz.rental.model.agreements.RentalEntity;

import java.util.List;

public interface RentalRevenueService {
    List<RentalRevenueDto> findAll();

    RentalRevenueDto findById(Long id);

    String getSumOfRentalRevenues();

    String getSumOfAllRevenues();

    Page<RentalRevenueDto> findPaginated(Pageable pageable);

    void createRentalRevenue(RentalEntity rentalEntity);

}
