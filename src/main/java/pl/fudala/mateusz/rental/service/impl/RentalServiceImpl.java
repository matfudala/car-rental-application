package pl.fudala.mateusz.rental.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.fudala.mateusz.rental.dto.agreements.RentalDto;
import pl.fudala.mateusz.rental.dto.agreements.ReservationDto;
import pl.fudala.mateusz.rental.model.agreements.RentalEntity;
import pl.fudala.mateusz.rental.model.agreements.ReservationEntity;
import pl.fudala.mateusz.rental.repository.RentalRepository;
import pl.fudala.mateusz.rental.repository.ReservationRepository;
import pl.fudala.mateusz.rental.repository.ReservationRevenueRepository;
import pl.fudala.mateusz.rental.service.RentalRevenueService;
import pl.fudala.mateusz.rental.service.RentalService;
import pl.fudala.mateusz.rental.utils.mapper.Mapper;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RentalServiceImpl implements RentalService {

    private final RentalRepository rentalRepository;
    private final ReservationRepository reservationRepository;
    private final ReservationRevenueRepository reservationRevenueRepository;
    private final RentalRevenueService rentalRevenueService;

    @Autowired
    public RentalServiceImpl(RentalRepository rentalRepository, ReservationRepository reservationRepository, ReservationRevenueRepository reservationRevenueRepository, RentalRevenueService rentalRevenueService) {
        this.rentalRepository = rentalRepository;
        this.reservationRepository = reservationRepository;
        this.reservationRevenueRepository = reservationRevenueRepository;
        this.rentalRevenueService = rentalRevenueService;
    }


    @Override
    public void convertReservationToRental(Long id, String message) {
        Optional<ReservationEntity> optRes = reservationRepository.findById(id);
        ReservationEntity res;
        if (optRes.isPresent()) {
            res = reservationRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        } else {
            return;
        }
        List<RentalEntity> all = rentalRepository.findAll();
        for (RentalEntity rentalEntity : all) {
            if (rentalEntity.getReservation().getId().equals(id)) {
                return;
            }
        }
        RentalEntity rentalEntity = new RentalEntity();
        rentalEntity.setRentalDate(res.getRentalStartDate());
        rentalEntity.setReservation(res);
        reservationRevenueRepository.getReservationRevenueByReservationId(id).setRealised(true);
        RentalEntity newRental = rentalRepository.save(rentalEntity);
        rentalRevenueService.createRentalRevenue(newRental);
    }

    @Override
    public List<RentalDto> getAllRentals() {
        return rentalRepository.findAll().stream().map(Mapper::map).collect(Collectors.toList());
    }

    @Override
    public RentalDto getRentalById(Long id) {
        return Mapper.map(rentalRepository.findById(id).orElseThrow(IllegalArgumentException::new));
    }

}

