package pl.fudala.mateusz.rental.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.fudala.mateusz.rental.dto.revenues.ReturnRevenueDto;
import pl.fudala.mateusz.rental.repository.ReturnRevenueRepository;
import pl.fudala.mateusz.rental.service.ReturnRevenueService;
import pl.fudala.mateusz.rental.utils.mapper.Mapper;

import java.math.BigDecimal;

@Service
public class ReturnRevenueServiceImpl implements ReturnRevenueService {

    private final ReturnRevenueRepository returnRevenueRepository;

    public ReturnRevenueServiceImpl(ReturnRevenueRepository returnRevenueRepository) {
        this.returnRevenueRepository = returnRevenueRepository;
    }

    @Override
    public Page<ReturnRevenueDto> findAll(Pageable pageable) {
        return returnRevenueRepository.findAll(pageable).map(Mapper::map);
    }

    @Override
    public BigDecimal sumOfSurcharges() {
        if (returnRevenueRepository.getSumOfRentalRevenues().isPresent()) {
            returnRevenueRepository.getSumOfRentalRevenues().get()
                    .setScale(BigDecimal.ROUND_CEILING, 2);
        }
        return new BigDecimal(0);
    }
}
