package pl.fudala.mateusz.rental.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import pl.fudala.mateusz.rental.dto.car.CarDto;

import java.time.LocalDateTime;
import java.util.List;

public interface CarService {
    List<CarDto> findByMake(String make);
    CarDto findById(Long id);
    void addCar(CarDto carDto);
    void editCar(CarDto carDto, Long id);
    void deleteCarById(Long id);
    List<CarDto> findAll();
    Page<CarDto> findAll(Pageable pageable);
    List<CarDto> findAvailableCarsByDepartment(Long departmentId);
    List<CarDto> findAllReservedCarsInProvidedDate(LocalDateTime dateTime);
    List<CarDto> findAllRentedCarsInProvidedDate(LocalDateTime dateTime);
    List<CarDto> findAllUnavailableCarsInProvidedDate(LocalDateTime dateTime);
    List<Long> getListOfIDsFromCarDtoList(List<CarDto> carDtoList);
    List<CarDto> getListOfCarsOnProvidedDate(LocalDateTime dateTime);
}
