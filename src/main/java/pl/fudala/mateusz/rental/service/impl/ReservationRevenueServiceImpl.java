package pl.fudala.mateusz.rental.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import pl.fudala.mateusz.rental.dto.revenues.ReservationRevenueDto;
import pl.fudala.mateusz.rental.model.agreements.ReservationEntity;
import pl.fudala.mateusz.rental.model.revenues.ReservationRevenueEntity;
import pl.fudala.mateusz.rental.repository.ReservationRepository;
import pl.fudala.mateusz.rental.repository.ReservationRevenueRepository;
import pl.fudala.mateusz.rental.service.ReservationRevenueService;
import pl.fudala.mateusz.rental.utils.mapper.Mapper;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ReservationRevenueServiceImpl implements ReservationRevenueService {

    private final ReservationRevenueRepository reservationRevenueRepository;

    @Autowired
    public ReservationRevenueServiceImpl(ReservationRevenueRepository reservationRevenueRepository) {
        this.reservationRevenueRepository = reservationRevenueRepository;
    }

    @Override
    public List<ReservationRevenueDto> findAll() {
        return reservationRevenueRepository.findAll()
                .stream()
                .map(Mapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public List<ReservationRevenueDto> findActive() {
        return reservationRevenueRepository.getActiveReservationRevenues()
                .stream()
                .map(Mapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public List<ReservationRevenueDto> findCancelled() {
        return reservationRevenueRepository.getCancelledReservationRevenues()
                .stream()
                .map(Mapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public ReservationRevenueDto findById(Long id) {
        return Mapper.map(reservationRevenueRepository.findById(id).orElseThrow(IllegalArgumentException::new));
    }

    @Override
    public String getSumOfReservationRevenues() {
        Optional<BigDecimal> revenues = reservationRevenueRepository.getSumOfReservationRevenues();
        if (revenues.isPresent()) {
            BigDecimal bigDecimal = revenues.orElseThrow(IllegalArgumentException::new);
            return String.valueOf(bigDecimal.setScale(BigDecimal.ROUND_CEILING, 2));
        }
        return String.valueOf(0);
    }

    @Override
    public String getSumOfActiveReservationRevenues() {
        Optional<BigDecimal> revenues = reservationRevenueRepository.getSumOfActiveReservationRevenues();
        if (revenues.isPresent()) {
            BigDecimal bigDecimal = revenues.orElseThrow(IllegalArgumentException::new);
            return String.valueOf(bigDecimal.setScale(BigDecimal.ROUND_CEILING, 2));
        }
        return String.valueOf(0);

    }

    @Override
    public String getSumOfCancelledReservationRevenues() {
        Optional<BigDecimal> revenues = reservationRevenueRepository.getSumOfCancelledRevenues();
        if (revenues.isPresent()) {
            BigDecimal bigDecimal = revenues.orElseThrow(IllegalArgumentException::new);
            return String.valueOf(bigDecimal.setScale(BigDecimal.ROUND_CEILING, 2));
        }
        return String.valueOf(0);
    }

    @Override
    public Page<ReservationRevenueDto> findPaginated(Pageable pageable) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<ReservationRevenueDto> list;
        List<ReservationRevenueDto> allReservationRevenues =
                reservationRevenueRepository.findAll()
                        .stream()
                        .map(Mapper::map)
                        .collect(Collectors.toList());
        if (allReservationRevenues.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allReservationRevenues.size());
            list = allReservationRevenues.subList(startItem, toIndex);
        }
        return new PageImpl<>(list,
                PageRequest.of(currentPage, pageSize),
                allReservationRevenues.size());
    }

    @Override
    public void addReservationRevenue(ReservationEntity reservationEntity) {
        ReservationRevenueEntity reservationRevenueEntity = new ReservationRevenueEntity();
        reservationRevenueEntity.setReservationValue(reservationEntity.getPrice());
        reservationRevenueEntity.setCancelled(false);
        reservationRevenueEntity.setRealised(false);
        reservationRevenueEntity.setReservationId(reservationEntity.getId());
        reservationRevenueRepository.save(reservationRevenueEntity);
    }

    @Override
    public void calculateRevenuesWhenCancelRes(ReservationEntity reservationEntity) {
        LocalDateTime startRentalDate = reservationEntity.getRentalStartDate();
        LocalDateTime cancelDate = LocalDateTime.now();
        Duration between = Duration.between(cancelDate, startRentalDate);
        long duration = between.toDays();
        ReservationRevenueEntity resRevenue = reservationRevenueRepository.getReservationRevenueByReservationId(reservationEntity.getId());
        if (duration <= 2) {
            resRevenue.setReservationValue(resRevenue.getReservationValue().multiply(BigDecimal.valueOf(0.2)));
            resRevenue.setCancelled(true);
        } else {
            resRevenue.setReservationValue(BigDecimal.ZERO);
            resRevenue.setCancelled(true);
        }
    }
}
