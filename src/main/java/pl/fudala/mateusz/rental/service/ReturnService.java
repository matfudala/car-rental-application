package pl.fudala.mateusz.rental.service;

import pl.fudala.mateusz.rental.dto.agreements.RentalDto;
import pl.fudala.mateusz.rental.dto.agreements.ReturnDto;

import java.util.List;

public interface ReturnService {

    void returnCar(RentalDto rentalDto);

    List<ReturnDto> allReturns();

}
