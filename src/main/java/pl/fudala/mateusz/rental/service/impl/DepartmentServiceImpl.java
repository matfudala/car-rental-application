package pl.fudala.mateusz.rental.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.fudala.mateusz.rental.dto.company.DepartmentDto;
import pl.fudala.mateusz.rental.model.car.CarEntity;
import pl.fudala.mateusz.rental.model.company.DepartmentEntity;
import pl.fudala.mateusz.rental.repository.CarRepository;
import pl.fudala.mateusz.rental.repository.DepartmentRepository;
import pl.fudala.mateusz.rental.service.DepartmentService;
import pl.fudala.mateusz.rental.utils.mapper.Mapper;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DepartmentServiceImpl implements DepartmentService {

    private final DepartmentRepository departmentRepository;
    private final CarRepository carRepository;

    @Override
    public List<DepartmentDto> findAll() {
        return departmentRepository.findAll().stream().map(Mapper::map).collect(Collectors.toList());
    }

    @Override
    public Page<DepartmentDto> findAll(Pageable pageable) {
        return departmentRepository.findAll(pageable).map(Mapper::map);
    }

    @Override
    public void addNewDepartment(DepartmentDto departmentDto) {
        departmentRepository.save(Mapper.map(departmentDto));
    }

    @Override
    public DepartmentDto findById(Long id) {
        return Mapper.map(departmentRepository.findById(id).orElseThrow(IllegalArgumentException::new));
    }

    @Transactional
    @Override
    public void editDepartment(DepartmentDto departmentDto, Long id) {
        List<CarEntity> cars = departmentDto.getCars();
        DepartmentEntity depEdited = departmentRepository.findById(departmentDto.getId()).orElseThrow(IllegalArgumentException::new);
        for (CarEntity car : depEdited.getCars()) {
            car.setDepartment(null);
        }
        List<Long> carsId = cars.stream()
                .map(CarEntity::getId)
                .collect(Collectors.toList());
        List<DepartmentEntity> departmentsWithCars = departmentRepository.findDepartmentsWithCars(id);
        for (DepartmentEntity departmentsWithCar : departmentsWithCars) {
            carsId.forEach(dtoCarId -> departmentsWithCar
                    .getCars().removeIf(x -> dtoCarId.equals(x.getId())));
        }
        DepartmentEntity departmentEntity = departmentRepository.findById(id).orElseThrow(IllegalArgumentException::new);
        departmentEntity.setDepartmentName(departmentDto.getDepartmentName());
        departmentEntity.setAddress(departmentDto.getAddress());

        departmentEntity.setCars(carRepository.findAllById(carsId));
        List<CarEntity> allCars = carRepository.findAll();
        List<DepartmentEntity> allDep = departmentRepository.findAll();
        for (DepartmentEntity dep : allDep) {
            for (CarEntity car : allCars) {
                if (dep.getCars().contains(car)) {
                    car.setDepartment(dep);
                }
            }
        }
        departmentEntity.setCars(carRepository.findAllById(carsId));
    }

    @Override
    public void deleteDepartmentById(Long id) {
        departmentRepository.deleteById(id);
    }

    @Override
    public List<DepartmentDto> findDepartmentsWithAvailableCars() {
        return departmentRepository.findDepartmentsWithAvailableCars().stream()
                .map(Mapper::map)
                .collect(Collectors.toList());
    }
}
