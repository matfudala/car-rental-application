package pl.fudala.mateusz.rental.service;

import pl.fudala.mateusz.rental.dto.person.CustomerDto;

public interface CustomerService {
    CustomerDto findById(Long id);
}
