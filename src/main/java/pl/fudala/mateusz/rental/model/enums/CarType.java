package pl.fudala.mateusz.rental.model.enums;

import lombok.Getter;

public enum CarType {
    ESTATE("Estate - family car"),
    HATCHBACK("Hatchback - city car"),
    SEDAN("Sedan - daily car"),
    SUV("SUV - 4-wheel-drive");

    @Getter
    private String description;

    CarType(final String description) {
        this.description = description;
    }
}
