package pl.fudala.mateusz.rental.model.company;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.fudala.mateusz.rental.model.person.EmployeeEntity;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Builder
@Table(name = "headquarter")
@NoArgsConstructor
@AllArgsConstructor
public class HeadquarterEntity {

    @Id
    private String name;
    private String website;
    private String companyLogo = "src/main/webapp/images/logo.jpg";

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id",
            foreignKey = @ForeignKey(name = "address_id_fk"))
    private AddressEntity address;

    @OneToOne
    private EmployeeEntity owner;

    @OneToMany(cascade = CascadeType.ALL)
    private List<DepartmentEntity> departments;
}
