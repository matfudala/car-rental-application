package pl.fudala.mateusz.rental.model.revenues;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity(name = "return_revenue")
@Builder
@Table(name = "return_revenue")
@NoArgsConstructor
@AllArgsConstructor
public class ReturnRevenueEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private BigDecimal returnSurcharge;
}
