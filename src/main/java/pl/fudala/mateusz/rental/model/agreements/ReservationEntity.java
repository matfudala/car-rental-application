package pl.fudala.mateusz.rental.model.agreements;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.fudala.mateusz.rental.model.car.CarEntity;
import pl.fudala.mateusz.rental.model.company.DepartmentEntity;
import pl.fudala.mateusz.rental.model.person.CustomerEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity(name = "reservation")
@Table(name = "reservation")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReservationEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDateTime reservationDate;
    @ManyToOne
    private CustomerEntity customer;
    @OneToOne
    private CarEntity car;
    private LocalDateTime rentalStartDate;
    private LocalDateTime rentalEndDate;

    @ManyToOne
    @JoinColumn(name = "department_departure_id")
    private DepartmentEntity departureDepartment;
    @ManyToOne
    @JoinColumn(name = "department_arrival_id")
    private DepartmentEntity arrivalDepartment;

    private BigDecimal price;

}
