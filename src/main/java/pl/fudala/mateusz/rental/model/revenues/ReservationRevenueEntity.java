package pl.fudala.mateusz.rental.model.revenues;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity(name = "reservation_revenue")
@Builder
@Table(name = "reservation_revenue")
@NoArgsConstructor
@AllArgsConstructor
public class ReservationRevenueEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private BigDecimal reservationValue;
    private boolean cancelled;
    private boolean realised;

    private Long reservationId;


}
