package pl.fudala.mateusz.rental.model.revenues;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Entity(name = "rental_revenue")
@Builder
@Table(name = "rental_revenue")
@NoArgsConstructor
@AllArgsConstructor
public class RentalRevenueEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private BigDecimal rentalValue;
}
