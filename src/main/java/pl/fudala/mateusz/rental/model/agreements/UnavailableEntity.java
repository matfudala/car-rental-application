package pl.fudala.mateusz.rental.model.agreements;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.fudala.mateusz.rental.model.car.CarEntity;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@Entity(name = "unavailable")
@Table(name = "unavailable")
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class UnavailableEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private LocalDateTime startDate;
    private LocalDateTime endDate;

    @OneToOne
    private CarEntity car;

}
